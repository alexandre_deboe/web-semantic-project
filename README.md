Installation
============

Nécessite PHP (développé avec la version 7) et composer.

Executez la commande : "composer install"

Suivez les instructions et notamment les instructions pour la connexion à la base de données (laissez le nom par default et mettre le bon identifiant et mot de passe).

Le projet a été réalisé avec MySQL. Un fichier de base de données fictives est fourni à des fins de démonstration dans le dossier bdd.

Pour changer le type de bdd, il faut changer le parametre doctrine:dbal:driver dans le fichier app/config/config.yml.

Pour lancer le serveur, il faut executer la commande : "php bin/console server:start (serveur inclu).
Si ca ne fonctionne pas : "php bin/console server:run" (nécessite un serveur php)


Le site est ensuite accessible à l'adresse : localhost:8000.
