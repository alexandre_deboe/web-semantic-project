-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2017 at 05:13 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors_movies`
--

CREATE TABLE `actors_movies` (
  `person_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `actors_movies`
--

INSERT INTO `actors_movies` (`person_id`, `movie_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 5),
(1, 7),
(3, 1),
(3, 8),
(4, 3),
(4, 7),
(5, 3),
(5, 6),
(6, 4),
(6, 5),
(7, 4),
(7, 7),
(8, 3),
(8, 4),
(8, 6),
(8, 8),
(9, 4),
(9, 6);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id` int(11) NOT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` datetime NOT NULL,
  `synopsis` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `Title`, `year`, `synopsis`, `image`, `person_id`, `type`) VALUES
(1, 'Boum', '2007-11-17 00:00:00', 'Une super team de super héros qui font un film vraiment génial à voir.', 'images/justice.png', 2, 'thriller'),
(2, 'La triste histoire de monsieur Troit', '1993-01-08 00:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/triste.png', 3, 'drama'),
(3, 'Le Jour et la nuit', '1965-11-01 00:00:00', 'Suspendisse odio urna, eleifend quis elementum eu, sodales ut erat. Praesent interdum urna sapien, quis tincidunt lacus pulvinar sit amet. Nulla et facilisis erat, tincidunt venenatis est. Integer dictum nulla ut urna mattis dictum. Quisque scelerisque suscipit dui. Aliquam blandit, risus ut ullamcorper maximus, ex nibh luctus libero, in imperdiet dui tortor sit amet ex. Cras aliquet at arcu et tincidunt. Suspendisse enim massa, ultricies non magna eget, faucibus interdum tortor. Etiam ac felis non nulla accumsan dignissim. Duis ornare metus non nibh molestie, at rhoncus nisi consequat. Nulla nec pretium sapien. Duis pulvinar fermentum varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam ut diam vel metus tincidunt laoreet id sit amet urna. Aliquam in tincidunt mauris.', 'images/jour.png', 10, 'thriller'),
(4, 'L\'ile fantastique', '1998-11-08 00:00:00', 'Vivamus est arcu, convallis nec pharetra sit amet, sagittis mattis felis. Etiam non lacus sapien. Duis pellentesque, erat sit amet molestie ultricies, felis urna sagittis turpis, id semper purus augue ac libero. Fusce quis cursus urna, vitae pellentesque velit. Cras aliquet, justo quis consectetur accumsan, leo velit consectetur urna, vitae gravida nulla tortor in turpis. Nunc nec malesuada quam. Integer ornare quis diam semper dignissim. Maecenas erat erat, posuere at blandit et, vulputate non lorem. Aliquam a tempus nisl. Cras efficitur ornare nunc, sit amet malesuada mauris bibendum ut. Suspendisse potenti. Mauris ut fringilla dui, feugiat ultrices lacus. In nec leo sem. Aliquam fringilla ornare dictum.', 'images/fantastique.png', 3, 'action'),
(5, 'L\'ile imaginaire', '1978-11-08 00:00:00', 'Vivamus est arcu, convallis nec pharetra sit amet, sagittis mattis felis. Etiam non lacus sapien. Duis pellentesque, erat sit amet molestie ultricies, felis urna sagittis turpis, id semper purus augue ac libero. Fusce quis cursus urna, vitae pellentesque velit. Cras aliquet, justo quis consectetur accumsan, leo velit consectetur urna, vitae gravida nulla tortor in turpis. Nunc nec malesuada quam. Integer ornare quis diam semper dignissim. Maecenas erat erat, posuere at blandit et, vulputate non lorem. Aliquam a tempus nisl. Cras efficitur ornare nunc, sit amet malesuada mauris bibendum ut. Suspendisse potenti. Mauris ut fringilla dui, feugiat ultrices lacus. In nec leo sem. Aliquam fringilla ornare dictum.', 'images/fantastique.png', 3, 'drama'),
(6, 'La magique histoire de monsieur Moi', '2003-01-08 00:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/triste.png', 3, 'action'),
(7, 'Justice League', '2017-11-17 00:00:00', 'Une super team de super héros qui font un film vraiment génial à voir.', 'images/justice.png', 2, 'thriller'),
(8, 'Le lendemain!', '1975-11-01 00:00:00', 'Suspendisse odio urna, eleifend quis elementum eu, sodales ut erat. Praesent interdum urna sapien, quis tincidunt lacus pulvinar sit amet. Nulla et facilisis erat, tincidunt venenatis est. Integer dictum nulla ut urna mattis dictum. Quisque scelerisque suscipit dui. Aliquam blandit, risus ut ullamcorper maximus, ex nibh luctus libero, in imperdiet dui tortor sit amet ex. Cras aliquet at arcu et tincidunt. Suspendisse enim massa, ultricies non magna eget, faucibus interdum tortor. Etiam ac felis non nulla accumsan dignissim. Duis ornare metus non nibh molestie, at rhoncus nisi consequat. Nulla nec pretium sapien. Duis pulvinar fermentum varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam ut diam vel metus tincidunt laoreet id sit amet urna. Aliquam in tincidunt mauris.', 'images/jour.png', 10, 'comedie');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `biography` longtext COLLATE utf8_unicode_ci,
  `birth` datetime DEFAULT NULL,
  `death` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `name`, `image`, `type`, `biography`, `birth`, `death`) VALUES
(1, 'Martin Beauchamps', 'images/cruise.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL),
(2, 'Julia Cherti', 'images/roberts.png', 'director', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', '2017-02-08 00:00:00'),
(3, 'Tom Cruise', 'images/cruise.png', 'both', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL),
(4, 'Creighton Hale‎', 'images/jolie.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL),
(5, 'Douglas Gerrard‎', 'images/cruise.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', '2017-02-08 00:00:00'),
(6, 'Devon Murray‎', 'images/roberts.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL),
(7, 'Pat Shortt‎', 'images/jolie.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL),
(8, 'Lumsden Hare', 'images/cruise.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', '2017-02-08 00:00:00'),
(9, 'Jean Hersholt‎ ', 'images/roberts.png', 'actor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', '2017-02-08 00:00:00'),
(10, 'Michael Gambon‎ ', 'images/jolie.png', 'director', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-02-08 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `years` datetime NOT NULL,
  `types` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors_movies`
--
ALTER TABLE `actors_movies`
  ADD PRIMARY KEY (`person_id`,`movie_id`),
  ADD KEY `IDX_B3012DC0217BBB47` (`person_id`),
  ADD KEY `IDX_B3012DC08F93B6FC` (`movie_id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1D5EF26FEAF7576F` (`Title`),
  ADD KEY `IDX_1D5EF26F217BBB47` (`person_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `actors_movies`
--
ALTER TABLE `actors_movies`
  ADD CONSTRAINT `FK_B3012DC0217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3012DC08F93B6FC` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `movie`
--
ALTER TABLE `movie`
  ADD CONSTRAINT `FK_1D5EF26F217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
