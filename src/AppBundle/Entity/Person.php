<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var Text
     *
     * @ORM\Column(name="biography", type="text", nullable=true)
     */
    private $biography;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="birth", type="datetime", length=255, nullable=true)
     */
    private $birth;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="death", type="datetime", length=255, nullable=true)
     */
    private $death;

    /**
     * @ORM\OneToMany(targetEntity="Movie", mappedBy="director")
     */
    private $directorMovies;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Movie", inversedBy="actors")
     * @ORM\JoinTable(name="actors_movies")
     */
    private $movies;


    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->movies = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Person
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Person
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set directorMovies
     *
     * @param ArrayCollection $directorMovies
     *
     * @return ArrayCollection
     */
    public function setDirectorMovies($directorMovies)
    {
        $this->directorMovies = $directorMovies;

        return $this;
    }

    /**
     * Get directorMovies
     *
     * @return ArrayCollection
     */
    public function getDirectorMovies()
    {
        return $this->directorMovies;
    }

    /**
     * Add directorMovie
     *
     * @param \AppBundle\Entity\Movie $directorMovie
     *
     * @return Person
     */
    public function addDirectorMovie(\AppBundle\Entity\Movie $directorMovie)
    {
        $this->directorMovies[] = $directorMovie;

        return $this;
    }

    /**
     * Remove directorMovie
     *
     * @param \AppBundle\Entity\Movie $directorMovie
     */
    public function removeDirectorMovie(\AppBundle\Entity\Movie $directorMovie)
    {
        $this->directorMovies->removeElement($directorMovie);
    }

    /**
     * Add movie
     *
     * @param \AppBundle\Entity\Movie $movie
     *
     * @return Person
     */
    public function addMovie(\AppBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \AppBundle\Entity\Movie $movie
     */
    public function removeMovie(\AppBundle\Entity\Movie $movie)
    {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Person
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set biography
     *
     * @param string $biography
     *
     * @return Person
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * Get biography
     *
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return Person
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set death
     *
     * @param \DateTime $death
     *
     * @return Person
     */
    public function setDeath($death)
    {
        $this->death = $death;

        return $this;
    }

    /**
     * Get death
     *
     * @return \DateTime
     */
    public function getDeath()
    {
        return $this->death;
    }
}
