<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Person;
use AppBundle\Entity\Search;
use AppBundle\Entity\Movie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class HomeController extends Controller
{
    /**
     * @Route("/search", name="homepage")
     */
    public function homeAction(Request $request)
    {
        $actors = $this->getDoctrine()
        ->getRepository(Person::class)
        ->findByType(array("actor", "both"));

        $directors = $this->getDoctrine()
        ->getRepository(Person::class)
        ->findByType(array("director", "both"));

        $search = new Search();

        $form = $this->createFormBuilder($search)
            ->add('directors', EntityType::class, array(
                'class' => 'AppBundle:Person',
                'choice_label' => 'name',
                'choices' => $directors,
                'multiple' => true,
                'required' => false
            ))
            ->add('actors', EntityType::class, array(
                'class' => 'AppBundle:Person',
                'choice_label' => 'name',
                'choices' => $actors,
                'multiple' => true,
                'required' => false
            ))
            ->add('years', ChoiceType::class, array(
                'choices' => array(
                '2010-' => new \DateTime("2010"),
                '2000-2010' => new \DateTime("2000"),
                '1990-2000' => new \DateTime("1990"),
                '1980-1990' => new \DateTime("1980"),
                '1970-1980' => new \DateTime("1970"),
                '1960-1970' => new \DateTime("1960"),
                ),
                'required' => false
                ))
            ->add('types', ChoiceType::class, array(
                'choices'  => array(
                'Comédie' => "comedie",
                'Dessin-animé' => "anime",
                'Comédie-dramatique' => "drama",
                'Thriller' => "thriller",
                'Action' => "action",
                ),
                'required' => false
                ))
            ->add('save', SubmitType::class, array('label' => 'Rechercher'))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                // $form->getData() holds the submitted values
                // but, the original `$task` variable has also been updated
            $search = $form->getData();
            
            $repository = $this->getDoctrine()
                ->getRepository('AppBundle:Movie');

            $parameters = array();

            if ($search->getYears() != null) {
                $year1 = $search->getYears();
                $year2 = (new \DateTime ($year1->format('Y-m-d')))->modify('+10 year');
                $yearfilter = 'p.year > :years AND p.year < :years2';
                $parameters['years'] = $year1;
                $parameters['years2'] = $year2;
            } else {
                $yearfilter = null;
            }

            if ($search->getTypes() != null) {
                $typefilter = 'p.type = :types';
                $parameters['types'] = $search->getTypes();
            } else {
                $typefilter = null;
            }

            if (!$search->getActors()->isEmpty()) {
                $actorfilter = ':actors MEMBER OF p.actors';
                $parameters['actors'] = $search->getActors();
            } else {
                $actorfilter = null;
            }

            if (!$search->getDirectors()->isEmpty()) {
                $directorfilter = 'p.director IN(:directors)';
                $parameters['directors'] = $search->getDirectors();
            } else {
                $directorfilter = null;
            }
            
            $query = $repository->createQueryBuilder('p')
                ->where($actorfilter)
                ->andwhere($directorfilter)
                ->andwhere($typefilter)
                ->andwhere($yearfilter)
                ->andwhere('1=1')
                ->setParameters($parameters)
                ->getQuery();
                
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                3/*limit per page*/
            );

        } else {
            $repository = $this->getDoctrine()
                ->getRepository(Movie::class);
            
            $query = $repository->createQueryBuilder('p')
                ->getQuery();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                3/*limit per page*/
            );
        }
        

        return $this->render('index.html.twig', array(
            'form' => $form->createView(),
            'actors' => $actors,
            'directors' => $directors,
            'pagination' => $pagination,
        ));
            
    }
}
