<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Movie;

class MovieController extends Controller
{
    /**
     * @Route("/movie/{id}", requirements={"id" = "\d+"})
     */
    public function homeAction($id)
    {
        $film = $this->getDoctrine()
        ->getRepository(Movie::class)
        ->findOneById($id);

        $sames = $this->getDoctrine()
        ->getRepository(Movie::class)
        ->findByType($film->getType());

        return $this->render('movie.html.twig', array(
            'movie' => $film,
            'sames' => $sames,
            ''
        ));
            
    }
}
