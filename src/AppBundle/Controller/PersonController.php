<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Person;

class PersonController extends Controller
{
    /**
     * @Route("/person/{id}", requirements={"id" = "\d+"})
     */
    public function homeAction($id)
    {
        $person = $this->getDoctrine()
        ->getRepository(Person::class)
        ->findOneById($id);

        return $this->render('person.html.twig', array(
            'person' => $person,
        ));
            
    }
}
